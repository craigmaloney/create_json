from setuptools import setup, find_packages
import sys, os

here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, 'README.rst')).read()
NEWS = open(os.path.join(here, 'NEWS.txt')).read()


version = '0.1'

install_requires = [
        'mutagen'
]


setup(name='create_json',
    version=version,
    description="Creates a JSON playlist file based on music file metadata",
    long_description=README + '\n\n' + NEWS,
    classifiers=[
      # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
    ],
    keywords='json shownotes',
    author='Craig Maloney',
    author_email='craig@decafbad.net',
    url='http://decafbad.net',
    license='GPL',
    packages=[''],
    #packages=find_packages('src'),
    package_dir = {'': 'src'},include_package_data=True,
    zip_safe=False,
    install_requires=install_requires,
    entry_points={
        'console_scripts':
            ['create_json=create_json:main']
    }
)
